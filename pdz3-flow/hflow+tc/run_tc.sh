#! /bin/bash -e
# Run curp inter-residue thermal conductivity calculations

nproc=${1:-2}

# postprocess
time mpiexec -n $nproc curp cal-tc \
    --frame-range 1 50 1 --average-shift 1 \
    -a outdata/acf.nc \
    -o outdata/ec.dat outdata/flux_grp.nc > hc.log
