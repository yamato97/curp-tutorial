#! /bin/bash -e
# Run curp heat flow calculations

nproc=${1:-2}

mkdir -p outdata
rm -f outdata/*.dat*

# calculate energy flux
time mpiexec -n $nproc curp compute flow.cfg > hflow.log

