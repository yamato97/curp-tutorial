#! /bin/bash -e
# Run curp energy flow calculations

nproc=${1:-2}

mkdir -p outdata
rm -f outdata/*.dat*

# calculate energy flux
time mpiexec -n $nproc curp compute flow.cfg > eflow.log

