ec_fp =  ../apo.ec.dat
fix_resnums = 1:306
other_opts = --ratio 0.5 --direction TB \
			 # -p 340:375 329:355

thresh = 0.008
thresh_weak = 0.003

line_values  = 0.015 0.008  0.003
line_colors  = red   blue   green
line_thicks  = 4.0   4.0     2.0
line_weights =   5     3       1
