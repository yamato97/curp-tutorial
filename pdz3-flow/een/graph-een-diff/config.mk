ec_fp1 = ../graph-een-apo/outdata/sel.ec.dat
ec_fp2 = ../graph-een-a3rem/outdata/sel.ec.dat

dval = 0.0001
thresh = 0.003
line_values  = 0.015 0.008 0.003
line_colors  = red   blue  green
line_thicks  = 4.0   4.0   2.0
line_weights =   5     3     1

other_opts = --ratio 0.3 --direction TB
